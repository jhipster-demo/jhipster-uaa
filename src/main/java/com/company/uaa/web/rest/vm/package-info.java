/**
 * View Models used by Spring MVC REST controllers.
 */
package com.company.uaa.web.rest.vm;
